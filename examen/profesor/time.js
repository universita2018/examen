
var timeInitial=document.getElementById("inicioEx").value;
var timeFinal=document.getElementById("finEx").value();
function calculaTime(id){
    var timeFuture=new Date(timeInitial);
    var timeActual=new Date();
    var days=0;
    var hours=0;
    var minutes=0;
    var seconds=0;
    if(timeFuture>timeActual){
        var diferencia=(timeFuture.getTime()-timeActual.getTime())/1000;
        days=Math.floor(diferencia/86400);
        diferencia=diferencia-(86400*days);
        hours=Math.floor(diferencia/3600);
        diferencia=diferencia-(3600*hours);
        minutes=Math.floor(diferencia/60);
        diferencia=diferencia-(60*minutes);
        seconds=Math.floor(diferencia);
        document.getElementById(id).innerHTML='Quedan ' + days + ' D&iacute;as, ' + hours + ' Horas, ' + minutes + ' Minutos, ' + seconds + ' Segundos';

        if (days>0 || hours>0 || minutes>0 || seconds>0){
            setTimeout("calculaTime(\"" + id + "\")",1000);
        }
    }
    else{
        document.getElementById('restante').innerHTML='Quedan ' + days + ' D&iacute;as, ' + hours + ' Horas, ' + minutes + ' Minutos, ' + seconds + ' Segundos';
    }

}
function closeExam() {
    var diferential=timeFinal-timeInitial;
    if (diferential==0){

        alert("Too Late");

        window.location.replace("index.php");
    }


}