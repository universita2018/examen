<?php
require_once("../conexion.php");

$result=mysqli_query($con,"select* from answer");
$col=1;

?>
<h1>REGISTRO DE RESPUESTAS</h1>
<p><a href="insertQuestion.php">insertar respuesta</a></p>
<table width="97%" border="2">
    <tr>
        <td width="7%" ><div align="center">id</div></td>
        <td width="17%"><div align="center">id pregunta</div></td>
        <td width="32%"><div align="center">respuesta</div></td>
        <td width="8%"><div align="center"> n° opc</div></td>
        <td width="11%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
    </tr>
    <?php while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
    if($col==1){echo "</tr>";	}?>
    <tr>

        <td align='middle'><?php echo $row['idAns']; ?></td>
        <td align='middle'><?php echo $row['idQ']; ?></td>
        <td align='middle'><?php echo $row['answer']; ?></td>
        <td><?php echo $row['rightAns']; ?></td>

        <td><a class="btn btn-primary" href="modifyAnswer.php?idAns=<?php echo $row['idAns'] ;?>">modificar</a></td>
        <td><a  class="btn btn-danger" href="deleteAns.php?idAns=<?php echo $row['idAns'] ;?>">eliminar</a></td>
        <?php }?>
    </tr>
</table>