<?php
require_once ("../conexion.php");



$sqs="SELECT * FROM exam";
$result=mysqli_query($con,$sqs);
$col=1;
?>
<!DOCTYPE html>
<html>
    <body>
        <h1>REGISTRO DE EXAMENES</h1>
        <p><a href="editExam.php">insertar examen</a></p>
        <table width="97%" border="2">
            <tr>
                <td width="7%" height="47" bgcolor="#33FF66"><div align="center">id</div></td>
                <td width="17%" bgcolor="#33FF66"><div align="center">idMateria</div></td>
                <td width="32%" bgcolor="#33FF66"><div align="center">tipoExamen</div></td>
                <td width="8%" bgcolor="#33FF66"><div align="center"> n° Exam</div></td>
                <td width="18%" bgcolor="#33FF66"><div align="center">fecha </div></td>
                <td width="18%" bgcolor="#33FF66"><div align="center">parcial</div></td>
                <td width="18%" bgcolor="#33FF66"><div align="center">duration</div></td>
                <td width="11%">&nbsp;</td>
                <td width="10%">&nbsp;</td>
            </tr>
            <?php while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            if($col==1){echo "</tr>";	} ?>
            <tr>

                <td align='middle'><?php echo $row['idExam']; ?></td>
                <td align='middle'><?php echo $row['idMat']; ?></td>
                <td align='middle'><?php echo $row['tipoExam']; ?></td>
                <td align='middle'><?php echo $row['numExam']; ?></td>
                <td align="middle"><?php echo $row['fechaExam']; ?></td>
                <td align='middle'><?php echo $row['parcial']; ?></td>
                <td align='middle'><?php echo $row['duration']; ?></td>
                <td><a href="modifyExam.php?idExam=<?php echo $row['idExam'] ;?>">modificar</a></td>
                <td><a href="deleteExam.php?idExam=<?php echo $row['idExam'] ;?>">eliminar</a></td>
                <?php }?>
            </tr>
        </table>
    </body>

</html>
