
<!DOCTYPE html>
<html>
    <head>
        <title>Table user</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/bootstrap.css">

        <link rel="stylesheet" href="../js/jquery.js">
        <script type="text/javascript" src="../js/jquery.js"></script>
    </head>

    <body>

        <h1>REGISTRO DE USUARIO</h1>
        <div>
            <button class="btn btn-primary" target="user">ADD</button>
            <table class="table" >
                <thead class="active">
                    <tr class="active">
                        <th>id</th>
                        <th>username</th>
                        <th>password</th>
                        <th> role</th>
                        <th>email</th>
                        <th>first name</th>
                        <th>last name</th>
                        <th>ci</th>
                        <th>address</th>
                        <th>phone</th>
                        <th width="11%">&nbsp;</th>
                        <th width="10%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody class="contenido">
                    <?php
                        include("showUser.php");
                    ?>
                </tbody>

            </table>
        </div>

        <script type="text/javascript" src="crudUser.js"></script>

            <div>
                <div>

                        <div>
                            <form action="<?php $_SERVER['PHP_SELF']; ?>"  enctype="multipart/form-data" method="post">
                                <link rel="stylesheet" href="../style2.css">
                                <label>Login:</label>
                                <input type="text" name="username" id="username"><br>
                                <label>Password:</label>
                                <input type="password" name="passwords" id="passwords"><br>
                                <label>role:</label>
                                <select id="role" name="role"><br>
                                    <option>Estudiante</option>
                                    <option>Profesor</option>
                                </select>
                                <label>email:</label>
                                <input type="text" name="email" id="email"><br>
                                <label>Nombre:</label>
                                <input type="text" name="fistName" id="fistName"><br>
                                <label>Apellido:</label>
                                <input type="text" name="lastName" id="lastName"><br>
                                <label>CI:</label>
                                <input type="number" name="ci" id="ci"><br>
                                <label>Direccion:</label>
                                <input type="text" name="address" id="address"><br>
                                <label>telefono:</label>
                                <input type="number" name="phone" id="phone"><br>
                                <button type="submit" class="btn btn-primary" data-dismiss="modal" id="registrar" value="registrar">registrar</button>

                            </form>
                        </div>
                </div>
            </div>

    </body>
</html>
