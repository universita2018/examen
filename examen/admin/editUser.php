<!DOCTYPE html>
<html>
    <head>
        <title>USER</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../js/jquery.js">
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script>
            var username,passwords,role,email,fistName,lastName,ci,address,phone;
            username = document.getElementById("username").value;
            passwords = document.getElementById("passwords").value;
            role = document.getElementById("role").value;
            email = document.getElementById("email").value;
            fistName = document.getElementById("fistName").value;
            lastName = document.getElementById("lastName").value;
            ci = document.getElementById("ci").value;
            address = document.getElementById("address").value;
            phone = document.getElementById("phone").value;
            function validar() {

                if(username==null){
                    alert("El campo esta vacio");
                }
                if( passwords == null  ) {
                    return false;
                }
                if( role == null  ) {
                    return false;
                }
                if( !(/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)/.test(email)) ) {
                    return false;
                }
                if( fistName == null  ) {
                    return false;
                }
                if( lastName == null  ) {
                    return false;
                }
                if( address == null  ) {
                    return false;
                }
                if( isNaN(ci) ) {
                    return false;
                }
                if( isNaN(phone) ) {
                    return false;
                }

            }
            function editUser() {
                $.ajax({
                    type: 'POST',
                    url:'../login.php',
                    date:('username'+username+&'passwords'+passwords+&'role'+role+&'email'+email+&'fistName'+fistName),
                    beforeSend:function () {
                        $('#imagen').show();
                        $('#mensajes').html('Procesando datos..');

                    },
                    success:function(respuesta) {
                        $('#imagen').hide();
                        if (respuesta==1) {
                            $('#mensajes').html('exito');
                            $('#username').val("");
                            $('#passwords').val("");

                        }
                        else {
                            $('#mensajes').html('No te has podido conectar');
                        }
                    }
                });
            }
        </script>

    </head>
    <body>
    <div class="container">
        <h1>REGISTRO</h1>
        <div class="formulario">
            <form action="" name="users" id="users" method="post">
                <link rel="stylesheet" href="../style2.css">
                <label>Login:</label>
                <input type="text" name="username" id="username"><br>
                <label>Password:</label>
                <input type="password" name="passwords" id="passwords"><br>
                <label>role:</label>
                <select id="role" name="role"><br>
                    <option>Estudiante</option>
                    <option>Profesor</option>
                </select>
                <label>email:</label>
                <input type="text" name="email" id="email"><br>
                <label>Nombre:</label>
                <input type="text" name="fistName" id="fistName"><br>
                <label>Apellido:</label>
                <input type="text" name="lastName" id="lastName"><br>
                <label>CI:</label>
                <input type="number" name="ci" id="ci"><br>
                <label>Direccion:</label>
                <input type="text" name="address" id="address"><br>
                <label>telefono:</label>
                <input type="number" name="phone" id="phone"><br>
                <input type="submit" id="enviar" value="Registrar">
                <div id="alert"><img id="imagen" src="../images/cargando-loading.gif.gif" alt=""><span id="mensajes"></span>Cargando..</div>
            </form>
        </div>
    </div>

    </body>
</html>