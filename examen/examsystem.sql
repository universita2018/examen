-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-05-2018 a las 22:35:22
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examsystem`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdmin` int(11) NOT NULL,
  `email` char(50) DEFAULT NULL,
  `clave` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdmin`, `email`, `clave`) VALUES
(1, 'admin2018@gmail.com', 'administraU2018');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answer`
--

CREATE TABLE `answer` (
  `idAns` int(11) NOT NULL,
  `idQ` int(11) DEFAULT NULL,
  `answer` text,
  `rightAns` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course`
--

CREATE TABLE `course` (
  `idCourse` int(11) NOT NULL,
  `nameCourse` char(20) DEFAULT NULL,
  `horario` time DEFAULT NULL,
  `aula` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `course`
--

INSERT INTO `course` (`idCourse`, `nameCourse`, `horario`, `aula`) VALUES
(8, 'ciencias sociales', '22:03:00', 'a1'),
(9, '', '00:00:00', ''),
(10, '', '00:00:00', ''),
(11, 'matematica', '21:15:00', 'a10'),
(12, 'historia', '13:15:00', 'b14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exam`
--

CREATE TABLE `exam` (
  `idExam` int(11) NOT NULL,
  `idMateria` int(11) DEFAULT NULL,
  `tipoExam` char(10) DEFAULT NULL,
  `numExam` int(3) DEFAULT NULL,
  `fechaExam` date DEFAULT NULL,
  `parcial` char(10) DEFAULT NULL,
  `duration` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `idMat` int(11) NOT NULL,
  `idCourse` int(10) DEFAULT NULL,
  `nameMat` char(50) DEFAULT NULL,
  `priority` varchar(5) DEFAULT NULL,
  `enable` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`idMat`, `idCourse`, `nameMat`, `priority`, `enable`) VALUES
(1, 0, '', '', ''),
(2, 0, '', '', ''),
(3, 0, '', '', ''),
(4, 0, '', '', ''),
(5, 0, '', '', ''),
(6, 0, '', '', ''),
(7, 11, 'algebra', 'yes', 'no'),
(8, 0, '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notes`
--

CREATE TABLE `notes` (
  `idNote` int(11) NOT NULL,
  `idStudent` int(11) DEFAULT NULL,
  `idExam` int(11) DEFAULT NULL,
  `pointNote` int(11) DEFAULT NULL,
  `numAns` int(11) DEFAULT NULL,
  `finalNote` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parcial`
--

CREATE TABLE `parcial` (
  `idParcial` int(11) NOT NULL,
  `parcial` char(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parcial`
--

INSERT INTO `parcial` (`idParcial`, `parcial`) VALUES
(1, 'Primer Parcial'),
(2, 'Segundo Parcial'),
(3, 'Tercer Parcial'),
(4, 'segunda Instancia'),
(5, 'extemporam');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `period`
--

CREATE TABLE `period` (
  `idPeriod` int(11) NOT NULL,
  `inicioEx` datetime DEFAULT NULL,
  `finEx` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodexam`
--

CREATE TABLE `periodexam` (
  `idPeriodExam` int(11) NOT NULL,
  `idPeriod` int(11) DEFAULT NULL,
  `idExam` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `question`
--

CREATE TABLE `question` (
  `idQ` int(11) NOT NULL,
  `typeQ` char(20) NOT NULL,
  `question` char(100) DEFAULT NULL,
  `optionAns` int(10) DEFAULT NULL,
  `opctions` char(80) DEFAULT NULL,
  `Answers` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `question`
--

INSERT INTO `question` (`idQ`, `typeQ`, `question`, `optionAns`, `opctions`, `Answers`) VALUES
(1, '', '', 0, '', 0),
(2, '', '', 0, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questionexam`
--

CREATE TABLE `questionexam` (
  `idQEx` int(11) NOT NULL,
  `idQ` int(11) DEFAULT NULL,
  `idEx` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student`
--

CREATE TABLE `student` (
  `idStudent` int(11) NOT NULL,
  `nameStudent` char(50) DEFAULT NULL,
  `state` char(10) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `studentmater`
--

CREATE TABLE `studentmater` (
  `idSM` int(11) NOT NULL,
  `idStudent` int(10) DEFAULT NULL,
  `idMateria` int(10) DEFAULT NULL,
  `hourPass` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teacher`
--

CREATE TABLE `teacher` (
  `idTeacher` int(11) NOT NULL,
  `nameTeacher` char(50) DEFAULT NULL,
  `worktime` int(5) DEFAULT NULL,
  `setTime` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teachermater`
--

CREATE TABLE `teachermater` (
  `idteaM` int(11) NOT NULL,
  `idteacher` int(10) DEFAULT NULL,
  `idMateria` int(10) DEFAULT NULL,
  `hourPass` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `username` char(30) DEFAULT NULL,
  `passwords` char(20) DEFAULT NULL,
  `role` char(20) DEFAULT NULL,
  `email` char(50) DEFAULT NULL,
  `fistName` char(50) DEFAULT NULL,
  `lastName` char(50) DEFAULT NULL,
  `ci` int(20) DEFAULT NULL,
  `address` char(50) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`idUser`, `username`, `passwords`, `role`, `email`, `fistName`, `lastName`, `ci`, `address`, `phone`) VALUES
(1, '', '', '', '', '', '', 0, '', 0),
(2, '', '', '', '', '', '', 0, '', 0),
(3, '', '', '', '', '', '', 0, '', 0),
(4, '', '', '', '', '', '', 0, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valor`
--

CREATE TABLE `valor` (
  `id` int(11) NOT NULL,
  `questionNo` int(5) NOT NULL,
  `AnsCorrect` tinyint(1) DEFAULT '0',
  `Answer` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdmin`);

--
-- Indices de la tabla `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`idAns`);

--
-- Indices de la tabla `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`idCourse`);

--
-- Indices de la tabla `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`idExam`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`idMat`);

--
-- Indices de la tabla `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`idNote`);

--
-- Indices de la tabla `parcial`
--
ALTER TABLE `parcial`
  ADD PRIMARY KEY (`idParcial`);

--
-- Indices de la tabla `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`idPeriod`);

--
-- Indices de la tabla `periodexam`
--
ALTER TABLE `periodexam`
  ADD PRIMARY KEY (`idPeriodExam`);

--
-- Indices de la tabla `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`idQ`);

--
-- Indices de la tabla `questionexam`
--
ALTER TABLE `questionexam`
  ADD PRIMARY KEY (`idQEx`);

--
-- Indices de la tabla `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`idStudent`);

--
-- Indices de la tabla `studentmater`
--
ALTER TABLE `studentmater`
  ADD PRIMARY KEY (`idSM`);

--
-- Indices de la tabla `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`idTeacher`);

--
-- Indices de la tabla `teachermater`
--
ALTER TABLE `teachermater`
  ADD PRIMARY KEY (`idteaM`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`);

--
-- Indices de la tabla `valor`
--
ALTER TABLE `valor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `answer`
--
ALTER TABLE `answer`
  MODIFY `idAns` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `course`
--
ALTER TABLE `course`
  MODIFY `idCourse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `exam`
--
ALTER TABLE `exam`
  MODIFY `idExam` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `idMat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `parcial`
--
ALTER TABLE `parcial`
  MODIFY `idParcial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `period`
--
ALTER TABLE `period`
  MODIFY `idPeriod` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `periodexam`
--
ALTER TABLE `periodexam`
  MODIFY `idPeriodExam` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `question`
--
ALTER TABLE `question`
  MODIFY `idQ` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `student`
--
ALTER TABLE `student`
  MODIFY `idStudent` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `studentmater`
--
ALTER TABLE `studentmater`
  MODIFY `idSM` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `teacher`
--
ALTER TABLE `teacher`
  MODIFY `idTeacher` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `teachermater`
--
ALTER TABLE `teachermater`
  MODIFY `idteaM` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `valor`
--
ALTER TABLE `valor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
